# How to setup

The following instructions explain how to setup this project for Linux environment after clone it
from this repository.

## Requirements

This project was made with Django 3.0 and dependencies are managed in a virtual environment.
To create the virtual environment, you need to install Python 3 and Pipenv first. To manage the project, you need Git installed.

The following versions are in used :

- Python 3.8
- [Pipenv 2018.11.26](https://pypi.org/project/pipenv/)
- Git

It's better if you install Pipenv globally to get the command anywhere on your machine and in virtual environnements. In order to do it, use this command :
`sudo -H pip3 install -U pipenv`

## Clone the project

To clone this repository in your local machine, create a new folder and execute the following command inside :
`git clone git@gitlab.com:diet-optimizer/django_graphql_recipes.git`

## Virtual Environment

After installing Pipenv you should have `pipenv` command enable or, if it's not in path you can try `python -m pipenv env`.
To activate the virtual environment, go to your project folder (django-graphql-recipes/django-graphql-recipes) and
execute the following command in a prompt :

`pipenv shell`

All commands must be executed in an activated virtual environment.
If you want to exit the environment, execute :

`deactivate`

## Migrations

To create an up-to-date migration file :

`./manage.py makemigrations`

To apply the database migrations :

`./manage.py migrate`

## Run

Now, you can run the project with the server by executing this command :

`./manage.py runserver`

- Go to http://127.0.0.1:8000/graphql/ to use the GraphiQL debugger

