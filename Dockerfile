FROM python:3
ENV PYTHONUNBUFFERED=1
#RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
RUN pip install Pipfile
RUN pip install graphene_django
RUN pip install django-cors-headers
RUN pip install django-filter
RUN pip install django-graphql-auth
EXPOSE 2222 80
#COPY . /code/