from django.db import models

class Food(models.Model):
    model=models.TextField(null=True)
    fat=models.FloatField(null=True) 
    carb=models.FloatField(null=True) 
    calorie= models.FloatField(null=True) 
    protein=models.FloatField(null=True)  
    NDB_NO=models.PositiveIntegerField(null=True, unique=True)
    group_code=models.PositiveIntegerField(null=True) 
    desc=models.TextField(null=True)
    group_name=models.CharField(max_length=101,null=True)

    def __str__(self):
        return self.desc
    class Meta:
        ordering = ('desc',)

class Recipe(models.Model):
    servings=models.PositiveIntegerField(null=True)
    recipeName = models.CharField(max_length=100,null=True)
    recipeSource= models.URLField(max_length = 200,null=True)
    Saturated_Fat = models.FloatField(null=True)  
    Total_Calories = models.FloatField(null=True) 
    Iron = models.FloatField(null=True)  
    Total_Fat= models.FloatField(null=True)  
    Total_Sugars= models.FloatField(null=True) 
    Added_Sugars_included= models.FloatField(null=True) 
    Dietary_Fiber= models.FloatField(null=True) 
    Cholesterol= models.FloatField(null=True) 
    Vitamin_D= models.FloatField(null=True) 
    Carbohydrates= models.FloatField(null=True) 
    Potassium= models.FloatField(null=True) 
    Sodium= models.FloatField(null=True) 
    Protein= models.FloatField(null=True) 
    Calcium= models.FloatField(null=True) 
    price = models.FloatField(null=True) 
    recipePicture= models.URLField(max_length = 200,null=True)
    recipeIngre= models.CharField(max_length=100,null=True)
    breakfast_judge= models.CharField(max_length=100,null=True)
    model=models.TextField(null=True)

    def __str__(self):
        return self.recipeName

    class Meta:
        ordering = ('recipeName',)
