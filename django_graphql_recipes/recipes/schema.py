import graphene
from graphene import Node
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType, ObjectType
from django_graphql_recipes.recipes.models import Food, Recipe

# Create a GraphQL type for the food model
class FoodNode(DjangoObjectType):
    class Meta:
        model = Food
        interfaces = (Node,)
        filter_fields ={"group_name":['exact', 'icontains', 'istartswith']}
# Create a GraphQL type for the recipe model
class RecipeNode(DjangoObjectType):
    class Meta:
        model = Recipe
        interfaces = (Node,)
        filter_fields = {
            'Total_Calories':['exact', 'gt', 'lt'],
            'Carbohydrates':['exact', 'gt', 'lt'],
            'Protein': ['exact', 'gt', 'lt'],
            'recipeIngre':['icontains'],
            'breakfast_judge':['exact'],
            #'Lipids': ['exact', 'gt', 'lt'],
            #'recipeName': ['exact', 'icontains', 'istartswith'],
        }

# Create a Query type
class Query(object):
    recipe = Node.Field(RecipeNode)
    all_recipes = DjangoFilterConnectionField(RecipeNode)

    food = Node.Field(FoodNode)
    all_foods = DjangoFilterConnectionField(FoodNode)