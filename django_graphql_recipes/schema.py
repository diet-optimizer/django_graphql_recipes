import graphene

from graphql_auth.schema import UserQuery, MeQuery
from graphql_auth import mutations

import django_graphql_recipes.recipes.schema


class AuthMutation(graphene.ObjectType):
    register = mutations.Register.Field()
    verify_account = mutations.VerifyAccount.Field()
    token_auth = mutations.ObtainJSONWebToken.Field()
    update_account = mutations.UpdateAccount.Field()


class Query(UserQuery, MeQuery, django_graphql_recipes.recipes.schema.Query, graphene.ObjectType):
    # This class will inherit from multiple Queries
    # as we begin to add more apps to our project
    django_graphql_recipes.recipes.schema.Query,
    graphene.ObjectType,


class Mutation(AuthMutation, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
